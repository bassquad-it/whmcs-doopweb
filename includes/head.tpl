<!-- Styling -->
{if $templatefile eq "clientregister"}
    <link href="{$WEB_ROOT}/templates/{$template}/css/bootstrap-select.min.css" rel="stylesheet">
{/if}
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Raleway:400,700" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/all.min.css?v={$versionHash}" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/custom.css" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/bootstrap-slider.min.css" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/styles-modified.css" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/slick.css" rel="stylesheet">
<link href="{$WEB_ROOT}/templates/{$template}/css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9] -->
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <!-- WhatsHelp.io widget -->
  <script type="text/javascript">
    (function () {
        var options = {
            facebook: "1628954383821500", // Facebook page ID
            whatsapp: "+5215538641349", // WhatsApp number
            email: "hola@doopweb.agency", // Email
            sms: "+5215538641349", // Sms phone number
            call: "+525570989673", // Call phone number
            company_logo_url: "//storage.whatshelp.io/widget/2d/2d5c/2d5c164fb2cdc1bc4ad50fc256ca42da/22554749_1630112627039009_8187595905452281574_n.png", // URL of company logo (png, jpg, gif)
            greeting_message: "Hola, ¿cómo podemos ayudarte? Simplemente envíenos un mensaje ahora para obtener ayuda.", // Text of greeting message
            call_to_action: "¡Hola! ¿Tienes alguna duda? 😏", // Call to action
            button_color: "#129BF4", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp,sms,call,email" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
  </script>
  <!-- /WhatsHelp.io widget -->
  <script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '779b13f0e485c6e96f4ad641234f54794d8b5f12');
  </script>
<![endif]-->

<script type="text/javascript">
    var csrfToken = '{$token}',
        markdownGuide = '{lang key="markdown.title"}',
        locale = '{if !empty($mdeLocale)}{$mdeLocale}{else}en{/if}',
        saved = '{lang key="markdown.saved"}',
        saving = '{lang key="markdown.saving"}';
</script>
<script src="{$WEB_ROOT}/templates/{$template}/js/scripts.min.js?v={$versionHash}"></script>

{if $templatefile == "viewticket" && !$loggedin}
  <meta name="robots" content="noindex" />
{/if}
